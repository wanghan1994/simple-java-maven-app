FROM openjdk:8-jre-alpine

RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
  && echo 'Asia/Shanghai' >/etc/timezone

COPY target/my-app-1.0-SNAPSHOT.jar /pum/

COPY entrypoint.sh /


ENTRYPOINT ["/bin/sh","/entrypoint.sh"]
